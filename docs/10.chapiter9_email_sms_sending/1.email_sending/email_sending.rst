###########################
Email sending - SMTP module
###########################

*******
Meaning
*******

SMTP: Simple Mail Transfer Protocol


********************
Create email address
********************

For this test, I have created a new email address. 

What is important is to go to Security options and to add App passwords: 

.. image:: figures/gmail_app_password.png

*************
Send the mail
*************

.. code-block:: python

    sender = "udemypythonproject@gmail.com"
    receiver = "dzoni_996@yahoo.com"

     """Create a object from SMTP class. We need to specify the location of our email provider.Its different for different email provider"""
    connection = smtplib.SMTP("smtp.gmail.com") # hotmail: smtp.live.com   yahoo: smtp.mail.yahoo.com
    
    connection.starttls()  # Transport layer security. Message will be encrypted. This is to secure the connection.
    connection.login(user=sender, password=password)
    connection.sendmail(
        from_addr=sender,
        to_addrs=receiver,
        msg="Subject: Hello\n\nThis is the body of my email",) # Body of the mail is separated by \n\n
    connection.close

We can also send the mail using with command so we don't need to close connection: 

.. code-block:: python

    with smtplib.SMTP("smtp.gmail.com")  as connection:
        connection.starttls()
        connection.login(user=sender, password=password)
        connection.sendmail(
            from_addr=sender,
            to_addrs=receiver,
            msg="Subject: Hello\n\nThis is the body of my email",)