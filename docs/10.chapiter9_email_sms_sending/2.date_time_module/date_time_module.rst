################
Date time module
################

.. code-block:: python

    import datetime as dt

    now = dt.datetime.now()
    year = now.year
    month = now.month
    day_of_week = now.weekday() # 0 - Monday

    date_of_birth = dt.datetime(year=1996, month=7, day=9, hour=4)
    print(date_of_birth)

We can also use datetime.strftime to have time directly in the good format. 