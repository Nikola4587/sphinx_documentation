######################
Higher order functions
######################

*******
Example
*******

.. code-block:: python

        def add(number1, number2):
            return number1 + number2

        def calculator(number1, number2, function):
            return function(number1, number2)

    # Calling the function
    calculator(2, 3, add)

    # Higher Order Function. One function can work with one another function.

