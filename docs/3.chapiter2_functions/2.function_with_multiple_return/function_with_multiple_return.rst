#####################################
Functions with multiple return values
#####################################

*******
Example
*******

.. code-block:: python 

    # We can have multiple return 
    def format_name(f_name: str, l_name: str):
        if f_name == "" or l_name == "":
            return "You dind't provide your name."
        formatted_f_name = (f_name.title())
        formatted_l_name = (l_name.title())
        return f"{formatted_f_name} {formatted_l_name}"

    # When we call the function
    formatted_string = format_name(  input("What is your first name ?"),
                                    input("What is your last name ?"))
    print(formatted_string)



    