##################
Functions as input
##################

*******
Example
*******

.. code-block:: python 

    def function_a(something):
        # Do this 
        # Then, do that
    
    def function_b():
        #Do this

    # Functions as input
    function_a(function_b) # Dont need to have parantheses in the end.


