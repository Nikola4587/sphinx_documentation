====================
Chapiter3: Functions
====================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   1.function_with_one_output/function_with_one_output
   2.function_with_multiple_return/function_with_multiple_return
   3.recursion/recursion
   4.function_as_input/function_as_input
   5.higher_order_functions/higher_order_functions


