#########################
Functions with one output
#########################

*******
Example
*******

.. code-block:: python 

    # Functions with  one output 
    def format_name(f_name: str, l_name: str):
        formatted_f_name = (f_name.title())
        formatted_l_name = (l_name.title())
        return f"{formatted_f_name} {formatted_l_name}"

    # When we call the function
    formatted_string = format_name("NIKOLA", "Dimitrijevic")
    print(formatted_string)

