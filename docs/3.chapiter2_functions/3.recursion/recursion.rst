#########
Recursion 
#########

*******
Example
*******

If the user answer "n", the calculation will restart from beggining.

.. code-block:: python 

    def calculator():
        print(logo)
        continue_calculation = True
        num1 = float(input("What is the first number ?"))
        while continue_calculation is True:
            for key in operations:
                print(key)
            operation_symbol = input("Pick an operation from the line above: ")
            num2 = float(input("What is the next number ?"))
            answer = operations[operation_symbol](num1, num2)
            print(f"{num1} {operation_symbol} {num2} = {answer}")

            ask_user_to_continue_calculation = input(f"Type 'y' to continue calculating with {answer}, or type 'n' to start"
                                                    f"the new calculation: ")
            if ask_user_to_continue_calculation == "y":
                num1 = answer
                continue_calculation = True
            else:
                continue_calculation = False
                calculator()  # This is calling recursion
