Introduction
============

What is package
---------------

1. Package: a bunch of code that somebody else has writter which the goal is to acheive a objectif. A package can contain multiples files. 

2. PyPi: place where we can find, install and publish Python Packages. Link: https://pypi.org/

3. Install the package from pypi in PyCharm:

3.1. Go to File --> Settings 

  .. image:: figures/py_char_add_package_1.png
    :width: 800

3.2. Serach for the package and click on install button

  .. image:: figures/py_char_add_package_2.png
    :width: 800


Differentes ways of improving modules
-------------------------------------

1. Basic import: 

 .. code-block:: python
 
    import turtle # import Module

    tim_the_turtle = turtle.Turtle() # Turtle in capital is a class

2. from module import class 

 .. code-block:: python
 
    from turtle import Turtle # from turtle module import class Turtle 

    tim_the_turtle = Turtle() # Turtle in capital is a class

3. Aliasing modules 

 .. code-block:: python
 
    from turtle import Turtle as t # from turtle module import class Turtle, rename Turtle class to t

    tim_the_turtle = t.Turtle() # Turtle in capital is a class

Important
---------

For example, turtle is the module that is packaged with python standard library. 
Turtle module is in python standard library. If the module is not in python standard library then we will need to install  it from pipy for example.