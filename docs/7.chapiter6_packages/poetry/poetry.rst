Poetry
======

What is Poetry:
---------------

Python and dependency management. 

Link to documentation: https://python-poetry.org/docs/

How to install poetry: 
----------------------

Windows: 

1. Open power shell
2. Run this command (Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py -UseBasicParsing).Content | python -
3. Update the path to the python installation folder if needed. 

Basic commands:
---------------

| 1. **poetry --version**: get the current version of poetry
| 2. **poetry --uninstall**: delete the actual version of poetry
| 3. **poetry --init**: This command will generate the toml file. 
 In this file we will have all the packages that we need to install.
| 5. **poetry install**: This command will install the virtual env from .toml file.  
| 4. **poetry env info --path**: path to the virtual env

Helpful videos:
---------------
https://www.youtube.com/watch?v=547Jr26duHQ&ab_channel=QAatthePoint%7CCarlosKidman

Good to know:
-------------

| If we are using Visual Studio Code to generate documentation, open common propt command line., 
| This will run activate.bat and the we can run Sphinx.
