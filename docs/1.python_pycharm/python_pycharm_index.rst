##################
Python and PyCharm
##################

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   1.python/python
   2.pycharm/pycharm
