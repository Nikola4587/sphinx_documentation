######
Python
######

Link to download file: https://www.python.org/downloads/ 

************
Installation
************

 1. Run downloaded file.

 2. Choose customized Installation: 

 .. image:: figures/customize_installation.png

 3. Choose optional features:

  .. image:: figures/optional_features.png

 4. Choose advanced options:

 It is very important to update the install location to C:\Python38\

  .. image:: figures/advanced_options.png

 5. Update install location in "Environment Variables" on your computer:

  .. image:: figures/environment_variables_1.png

  .. image:: figures/environment_variables_2.png


