#########
Variables
#########

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   1.variables/variables
   2.environment_variable/environment_variable
   