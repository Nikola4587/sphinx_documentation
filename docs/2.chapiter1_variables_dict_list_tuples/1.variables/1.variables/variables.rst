#########
Variables
#########

*****
Scope
*****

Introduction
============

**Scope:** how the variables are seen or can be accessed in different parts of the programme. 


1. **Local scope:** variable used inside the function. Those variables are used only inside the function. 
2. **Global scope:**  variable defined at the top level of the file. We can use this variable everywhere in the file. (Not inside the function)
3. The only difference between local scope and global scope is where we define the variable. 
4. Local and global scope is not only applicable for variables. We can use it also for functions (everything that we can name.)

Local scope
===========

.. code-block:: python 

    enemies = 1

    def increase_enemies():
        enemies = 2

        # print how much enemies to we have inside the funciton. 
        print(f"enemies inside function {enemies}")

    # Print how much enemies we have outside of the function:
    increase_enemies()
    print(f"enemies outside the function {enemies}")    

    # Output 
    # 2 --> this number is coming from function increase_enemies.
    # 1 --> this number is coming from global variable.
    
**Conclusion:** We can see that the variable "enemies" has not been changed. 

Global Scope
============

Here is the solution if we would like to increase variable enemies:

.. code-block:: python 

    enemies = 1

    def increase_enemies():
        global enemies # we will need to precise that enemy is the global variable.
        enemies += 1

        # print how much enemies to we have inside the funciton.
        print(f"enemies inside function {enemies}")

    # Print how much enemies we have outside of the function:
    increase_enemies()
    print(f"enemies outside the function {enemies}")

    # Output
    # 2
    # 2

Recommendation
==============

| ITS NOT RECOMMENDED TO MODIFY GLOBALS VARIABLES BECAUSE IT CAN BE A SOURCE OF ISSUES.
| ITS RECOMMENDED TO USE GLOBALS VARIABLES ONLY WHEN WE WILL NEVER CHANGE IT.
