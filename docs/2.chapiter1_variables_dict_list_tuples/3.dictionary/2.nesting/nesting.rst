#######
Nesting
#######

**********************
Nesting representation
**********************

Nesting: mater of putting list and dictionary one inside the other

| We can not do this: 
| travel_log = {"France": "Paris", "Marseille", "Dijon"}
| EACH KEY CAN HAVE ONLY ONE VALUE!!!
| Only vay to do it is to turn it into a list: 

.. code-block:: python 

    travel_log = {
                "France": ["Paris", "Marseille", "Dijon"],
                "Germany": ["Berlin", "Hamburg", "Stuttgart"]
                }

************************************
Nesting a dictionary in a dictionary
************************************

.. code-block:: python 

    travel_log = {
                "France": {"cities_visited": ["Paris", "Lille", "Dijon"],"total_visits": 12},
                "Germany": {"cities_visited": ["Berlin", "Hamburg", "Stuttgart"], "total_visits": 5},
                }

******************************
Nesting a dictionary in a List
******************************

 .. image:: figures/nesting_dict_to_list.png
    :width: 800    
