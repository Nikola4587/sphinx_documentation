##########
Dictionary
##########

*******
Keyword
*******

+---------------+-----------+
| KEY           | VALUE     |          
+---------------+-----------+
| Bug           | "1234"    |
+---------------+-----------+

| In python: 
| my_dict = {"Bug": "1234"}

***************
Functionalities
***************

**Create dictionary**

.. code-block:: python 

   my_dict = {
            "Bug": "1234",
            "Functions": "5678",
            }


**Update dictionary** 

.. code-block:: python 

    my_dict.update({KEY: VALUE}) 
   

**Take the information from dictionary**

 my_dict["Bug"] # We need to provide the key to see the item.

**Add item to dictionary**

 my_dict["Loop"] = "987"

**Loop through dictionary**

.. code-block:: python 

    for key in my_dict:
        print(my_dict[key]) # We need to provide the key to see the item.

Please take a look to dictionary comprehension for better examples.
