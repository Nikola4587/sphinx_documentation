#######
Slicing
#######

*******
Meaning
*******

| Take only one part from a list. 
| To do it we can write a for loop and loop into a list and take only te part that we need. 

..TODO: Add example of for loop. 

*******
Example
*******

.. code-block:: python

    piano_keys = ["a","b", "c", "d", "e", "f", "g"]
    # We would like to take only b - d

    piano_keys[2:5] # result : c, d, e

    # We can also specify the increment
     piano_keys[2:5:2] # result: c, e
