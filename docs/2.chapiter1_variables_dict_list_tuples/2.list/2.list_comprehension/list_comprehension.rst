##################
List comprehension
##################

*******
Meaning
*******

**List comprehension:** create a new list from previous one. 
It can also be used for string range or tuples.

 .. image:: figures/list_comprehension_example.png
    :width: 800    

********
Keywords
********

.. code-block:: python 

    new_list = [new_item for item in list]
    new_list = [new_item for item in list if test]

********
Examples
********

Range
=====

**Keyword:** new_list = [new_item for item in list]

.. code-block:: python 

    new_numbers = [num*2 for num in range(1,5)]


Conditional list comprehension
==============================

**Keyword:** new_list = [new_item for item in list if test]

 .. image:: figures/conditional_list_comprehension_example.png
    :width: 800    