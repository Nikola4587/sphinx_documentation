=====
Lists
=====

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   1.slicing/slicing
   2.list_comprehension/list_comprehension
   
