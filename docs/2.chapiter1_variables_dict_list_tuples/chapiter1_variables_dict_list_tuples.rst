==================================================
Chapiter1: Variables, dictionary, list and tuples
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   1.variables/variables_index
   2.list/list_index
   3.dictionary/dictionary_and_nesting_index

