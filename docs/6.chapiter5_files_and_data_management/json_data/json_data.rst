#########
JSON data
#########

Type of json data is a dictionary.
Json data is better to use because is much more easier to search data into a json file compare to 
txt file.


*****
Write
*****

 json.dump()

.. code-block:: python

    new_data = {
        website_entered: {
            "email": 'email@email.com',
            "password": 'password',
        }
    }

    with open(file="data.json", mode="w") as file:
        json.dump(new_data, file, indent=4) # indent is just to make easier to read


****
Read
****

    json.load()    

.. code-block:: python

    with open(file="data.json", mode="r") as file:
       data = json.load(file)

******
Update
******

    json.update()

.. code-block:: python

    with open(file="data.json", mode="r") as file:
    # Read all data
    data = json.load(file)
    
    # Updating old data with new data
    data.update(new_data)
    
    # Saving updated data
    with open(file="data.json", mode="w") as file:
    json.dump(data, file, indent=4)  # indent is just to make easier to read