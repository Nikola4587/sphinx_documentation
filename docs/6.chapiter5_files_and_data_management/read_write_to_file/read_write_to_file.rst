======================
Read and write to file
======================

Read the file
-------------

Classic method: 

.. code-block:: python

    # Read and close the file
    file = open(file="my_file.txt")  # open the file
    contents = file.read() # Read the file
    print(contents) # Print what is in file 
    file.close() # Important to close

**Keyword with:**

.. code-block:: python

    with open(file="my_file.txt") as file:  # open and close the file automatically
    contents = file.read()
    print(contents)  # With keyword will automaticly close the file


Write to the file
-----------------

.. code-block:: python

    # Write the file
    with open(file="my_file.txt", mode="w") as file:  *# Important to update the mode to "w" -> write or "a" -> append
    file.write("New text.")
