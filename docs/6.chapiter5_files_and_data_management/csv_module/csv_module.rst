==========
CSV module
==========

Example
-------

There is a csv library that can help us to manage csv files. 

.. code-block:: python
    
    import csv

    with open(file="weather_data.csv", mode="r") as data_file:
        data = csv.reader(data_file)
        for row in data:
            print(row)
