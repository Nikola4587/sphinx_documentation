============================
Pandas: csv data meanagement
============================

Pandas library is Python's data analysis library.

Documentation link: https://pandas.pydata.org/docs/

.. code-block:: python

    import pandas

    data = pandas.read_csv(filepath_or_buffer="weather_data.csv")
    temp_value = data["temp"] # Pandas is using first row as the name of each column. 
    temp_value = data.temp # is also working


| Its much more easy to manage the csv files with pandas. 
| If we print "data", this is the output that we will have:

.. image:: figures/print_data_using_pandas.png
    :width: 200

Loop through rows of a data
---------------------------

for (index, row) in data.itterows(): # itterows: read contents of csv line by line
    print(row.name_of_row) # row=the contents of columns with index coming from for condition.


Pandas Series and Data Frame
----------------------------

Data frame is a file. 
Series is a row. 

Get data from specific row
--------------------------

.. code-block:: python

    # Get the data from a  specific row
    data = pandas.read_csv(filepath_or_buffer="weather_data.csv")
    print(data[data.day == "Monday"])

Create a data frame from strach
-------------------------------

.. code-block:: python

    data_dict = {
        "students": ["Amy", "James", "Angela"],
        "scores": [76, 56, 65]
    }

    datas = pandas.DataFrame(data_dict)

    # create the csv
    datas.to_csv("new_data.csv")

    print(datas)

output: 

.. image:: figures/data_created_using_pandas.png
    :width: 200


