====================================
Chapiter5: Files and Data management
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   read_write_to_file/read_write_to_file
   file_path/file_path
   csv_module/csv_module
   pandas_module/pandas_module
   json_data/json_data