#############
Documentation
#############

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   1.python_pycharm/python_pycharm_index
   2.chapiter1_variables_dict_list_tuples/chapiter1_variables_dict_list_tuples
   3.chapiter2_functions/function_index
   4.chapiter3_arguments/arguments_index
   5.chapiter4_poo/poo_index
   6.chapiter5_files_and_data_management/files_and_data_management_index
   7.chapiter6_packages/packages_index
   8.chapiter7_graphical_user_interface/graphical_user_interface_index
   9.chapiter8_exceptions/exceptions_index
   10.chapiter9_email_sms_sending/email_sms_sending_index
   11.chapiter10_api/api_introduction_index
   12.chapiter11_web_foundation/web_foundation_index
  


