######
Turtle
######

************
Introduction
************

************************
Basics commands - Turtle
************************

.. code-block:: python

    my_turtle = Turtle() # create a object from class
    my_turtle.shape(SHAPE)
    my_turtle.color(COLOR)
    my_turtle.turtlesize(stretch_len=LENGTH, stretch_wid=WIDTH)
    my_turtle.setheading(90) # rotate turtle
    my_turtle.penup()
    my_turtle.goto(self.position)
    my_turtle.forward(20)
    my_turtle.backward(20)

************************
Basics commands - Screen
************************

.. code-block:: python

    # Setup screen
    screen = Screen()
    screen.setup(width=value, height=value)
    screen.tracer(0)  # turn off the animation
    screen.bgcolor("color")
    screen.title("title")
    screen.exitonclick() # close the screen on click


*******************************
Listen to the key from keyboard
*******************************

.. code-block:: python

    screen.listen()  # Listen to the key from keyboard
    screen.onkey(fun=function, key="Up")