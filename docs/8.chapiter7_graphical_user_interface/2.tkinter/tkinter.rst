#######
Tkinter
#######

****************************************
Creating a new window and configurations
****************************************

.. code-block:: python 

    window = Tk()
    window.title("Widget Examples")
    window.minsize(width=500, height=500)

******
Labels
******
.. code-block:: python 

    label = Label(text="This is old text")
    label.config(text="This is new text")
    label.pack() # pack, grid or place

*******
Buttons
*******
.. code-block:: python 

        answer_false_image = tkinter.PhotoImage(file="images/false.png")
        button_false = tkinter.Button(image=answer_false_image, highlightthickness=0)  # x and y values. Allow us to center the image.
        button_false.grid(column=1, row=2)

***************************
Calls action() when pressed
***************************

.. code-block:: python 

    button = Button(text="Click Me", command=action)
    button.pack()

*******
Entries
*******

.. code-block:: python 

    entry = Entry(width=30)
    #Add some text to begin with
    entry.insert(END, string="Some text to begin with.")
    #Gets text in entry
    print(entry.get())
    entry.pack()

****
Text
****

.. code-block:: python 

    text = Text(height=5, width=30)
    #Puts cursor in textbox.
    text.focus()
    #Adds some text to begin with.
    text.insert(END, "Example of multi-line text entry.")
    #Get's current value in textbox at line 1, character 0
    print(text.get("1.0", END))
    text.pack()

*******
Spinbox
*******

.. code-block:: python 

    def spinbox_used():
        #gets the current value in spinbox.
        print(spinbox.get())
    spinbox = Spinbox(from_=0, to=10, width=5, command=spinbox_used)
    spinbox.pack()

*****
Scale
*****

.. code-block:: python 

    #Called with current scale value.
    def scale_used(value):
        print(value)
    scale = Scale(from_=0, to=100, command=scale_used)
    scale.pack()

***********
Checkbutton
***********

.. code-block:: python 

    def checkbutton_used():
        #Prints 1 if On button checked, otherwise 0.
        print(checked_state.get())
    #variable to hold on to checked state, 0 is off, 1 is on.
    checked_state = IntVar()
    checkbutton = Checkbutton(text="Is On?", variable=checked_state, command=checkbutton_used)
    checked_state.get()
    checkbutton.pack()

***********
Radiobutton
***********

.. code-block:: python 

    def radio_used():
        print(radio_state.get())
    #Variable to hold on to which radio button value is checked.
    radio_state = IntVar()
    radiobutton1 = Radiobutton(text="Option1", value=1, variable=radio_state, command=radio_used)
    radiobutton2 = Radiobutton(text="Option2", value=2, variable=radio_state, command=radio_used)
    radiobutton1.pack()
    radiobutton2.pack()

*******
Listbox
*******

.. code-block:: python 

    def listbox_used(event):
        # Gets current selection from listbox
        print(listbox.get(listbox.curselection()))

    listbox = Listbox(height=4)
    fruits = ["Apple", "Pear", "Orange", "Banana"]
    for item in fruits:
        listbox.insert(fruits.index(item), item)
    listbox.bind("<<ListboxSelect>>", listbox_used)
    listbox.pack()
    window.mainloop()

***********************************
Puts a image in background - Canvas
***********************************

.. code-block:: python 

    canvas = tkinter.Canvas(width=200, height=240, bg=YELLOW, highlightthickness=0) # highlightthickness=0 est le countour de la photo
    tomato_image = tkinter.PhotoImage(file="tomato.png")
    canvas.create_image(100, 112, image=tomato_image)  # x and y values. Allow us to center the image.
    canvas.create_text(100, 130, text="00:00", fill="white", font=(FONT_NAME, 35, "bold"))
    canvas.pack()

Update canvas

.. code-block:: python 

    self.canvas_question.itemconfig(self.question_text, text=question_text)

*****************
Counting the time
*****************

.. code-block:: python 

    # 1000 is ms, count_down is a function that will be called every second and then
    # we have arguments that we can pass to the function count_down
    window.after(1000, count_down, count - 1)

******
POP-UP
******

.. code-block:: python 
    
    #First we need to import message box from tkinter
    from tkinter import messagebox

    # Create a message box
    is_ok = messagebox.askokcancel(title=website_entered, message=f"These are the details entered: \n Email: {email_entered} \n 
    Password: {password_entered} \n Is it ok to save?")



*********
IMPORTANT
*********

Tkinter layout managers: pack, place and grid.

We can not mix pack and grid. We need to chose one or other.