####################
Introduction to html
####################

-----------------
Internet request:
-----------------

| 1. We search google.com on our computer
| 2. The browser sends a message to internet service provider (Swissom, Sunrise)
| 3. Service provider provide the message to DNS Server (domain name system server - like phone book). DNS server will lookup in the data base what is 
 the exact IP address of the website that we are trying to access (216.58.210.46).
| 4. When the DNS founds the IP address of the website, it sends back to out browser.
| 5. Then browser will send a direct request to IP address through our Internet service provider. 
| 6. Then this message is delivered on Internet BackBone()
| 7. The server that is located at that ip address will receive the request.
| 8. The server than sends back through internet service provider and we can read data.

The data that we receive from the server are in one of these 3 formats: 
| HTML: structure of the site (Add image, button, text box).
| CSS: styling the website.
| JS: allows website to do things.   


HTML: HyperText Markup Language 

The good website to start to create web pages is https://codepen.io/. 

--------
HEADINGS
--------

 .. image:: figures/headings.png

H1 is the biggest heading. More the number is bigger, the size of heading is smaller. H6 is the smallest heading.

| Documentation: 
| 1. Mozzila developper documentation: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements
| 2. W3 schools: https://www.w3schools.com/html/html_headings.asp 
| 3. Userful website: https://devdocs.io/html-elements/ 

HTML BASICS:
------------

.. code-block:: HTML

    <h1>Hello World!</h1>

    <h1> Start tag
    </h1> End tag
    Inside tags is the content.

HTML attribute: 

.. code-block:: HTML

    <hr size="3">

    hr -->  html element 
    size-"3" --> html attribute 

<br> Break line. Self closing tag. 
<hr> Line