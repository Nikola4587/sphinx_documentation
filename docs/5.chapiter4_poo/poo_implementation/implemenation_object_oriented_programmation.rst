Object oriented programming - Implementation
============================================

Class
-----

| 1. How to name a class: class naming use PascalCase. Every new word starts with a capital letter.

| 2. Others types of naming:
|  * snake case --> naming with space between each word (snake_case)
|  * camel case --> first letter lower and first letter of other words are capital.

| 3. Python example of creating a class:

 .. code-block:: python
 
    class User:  # create a class
 

Constructor
-----------

| 1. What is it ?
|  A part of blue print that allows us to specify what should happen when our object is beeing constructed. 
|  It is also nommed as initialitzation of object. 
|  It is used to set the counters and attributs to the starting values. 

| 2. How to create a constructor?
|  The way that we would create the constructor, is by using the special method which is __init__. 
|  INIT FUNCTION WILL BE CALLED EVERY TIME WE CREATE A NEW OBJECT FROM A CLASS.

| 3. Python example of creating a constructor:

  .. code-block:: python
 
    class User:  # create a class
      def __init__(self):  # constructor. Self: object that is beeing created or beeing initialized.
      # Initialize the attributs.

 .. image:: figures/poo_constructor.png
  :width: 800

Attributs
---------

| In addition of self, we can add many parameters as we wish. 
| That parameter will be passed when an object gets constructed from this class. 
| Once we received that data, then we can use them to set the objects attributs.

| **1. Add atributes to a class in the constructor:**

  .. code-block:: python

    class Car:  # create a class
          def __init__(self, seats):  # attributs are seats
            self.seats = seats

    # create a object from class
    my_car = Car(seats=5)

| **2.We can also need somethimes to have a starting value, and to modify it when needed.**
|  Example: We would like to create Instagram users, and we dont know how much followers we have.

 .. code-block:: python

    class User:
    def __init__(self, user_id, user_name):
        self.id = user_id
        self.user_name = user_name
        self.followers = 0  # default value

    # When we create a object from a class, dont pass followers attributs.
    user_1 = User(user_id="001", user_name="Nikola")

    # If we would like to change followers attributs, we can do like this: 
    user_1.followers = 55


Methodes
--------

.. code-block:: python

    class Car:  # create a class
        def __init__(self, seats):  # attributs are seats
                self.seats = seats

        def enter_race_mode(self):
        self.seats = 2  # modify object seats attribut

    my_car = Car()  # create a object from class
    my_car.enter_race_mode()  # call the method enter race mode

    
Class inteheriance
------------------

How its working: 

.. code-block:: python

  class Chef: 
    bake()
    stir()
    measure()

If we need a Pastry Chef, we dont need to start to create from 0. 
We can take the methodes from chef class and add new methodes.

.. code-block:: python

  class Pastry_Chef: 
    bake()
    stir()
    measure()
    knead()
    whish()

This process is called CLASS INTHERIANCE. 

How its working in the code:

.. code-block:: python

  class Fish(Animal):  # Here in the parantheses is the class that we would like to herite from.
    def __init__(self):
      super().__init__()

  # The Fish is inheriting from Animal class. 
  # To have acces on all what the animal IS and HAVE, inside the init we should add super().__init__().
  # Super refer to supper class which is Animal in this case.


# Example

.. code-block:: python

  class Animal:
      def __init__(self):
          self.num_eyes = 2

      def breathe(self):
          print("Inhale, exhale.")

  class Fish(Animal):
      def __init__(self):
          super().__init__() # The call to super() in the initialiser is recommended, but not strictly required.

      def breathe(self):
          super().breathe() # take the same method from super class Animal
          print("doing this underwater.")

      def swim(self):
          print("moving in water.")

  nemo = Fish()
  nemo.breathe()


   

