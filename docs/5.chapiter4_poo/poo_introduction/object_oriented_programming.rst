Object oriented programming - Introduction
==========================================

.. image:: figures/poo_graph.png
    :width: 800


From a class, we can have multiple objects. 

 .. code-block:: python

      Helene = Weiter()

      James = Weiter()

Create a object
---------------

 .. code-block:: python

  timmy = Turtle() # Parentheses are there to construct the object
  my_screen = Screen()

Acces to attributs
------------------

 .. code-block:: python

  print(my_screen.canvheight)


Call a methode
--------------

 .. code-block:: python

  timmy.color("coral") # pass the parametre coral to the method color

Turtle graphic interface: Link: https://docs.python.org/3/library/turtle.html




