############################
Unlimited key word arguments
############################

Unlimited key word arguments are used in case we don't know how much arguments 
needs to be used. 

*******
Example
*******

.. image:: figures/unlimited_key_word_argument_example.png


*******************
Example using class
*******************

.. image:: figures/unlimited_key_word_argument_example_using_class.png

In this case we provided make and model to the class. If we specify make but not the model the 
program will crash.

.. image:: figures/unlimited_key_word_argument_example_using_class_key_error.png

To correct this issue we can use kwargs.get(""). In this case, if we don't specify the value
None will be returned.

.. image:: figures/unlimited_key_word_argument_example_using_class_key_error_corrected.png