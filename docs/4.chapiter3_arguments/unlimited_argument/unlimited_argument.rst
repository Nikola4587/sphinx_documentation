###################
Unlimited arguments
###################

Unlimited arguments are used in case when we don't know how many arguments 
needs to be used.

*******
Example
*******

.. image:: figures/unlimited_argument_example.png


"*" means that the function add can accept any number of arguments. The numbers will be in a form of tuple.