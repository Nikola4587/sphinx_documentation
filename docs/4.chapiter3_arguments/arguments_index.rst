####################
Chapiter3: Arguments
####################

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   keyword_arguments/keyword_arguments
   arguments_default_values/arguments_default_values
   unlimited_argument/unlimited_argument
   unlimited_key_word_arguments/unlimited_key_word_arguments