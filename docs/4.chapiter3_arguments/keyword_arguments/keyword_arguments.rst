#################
Keyword arguments
#################

.. image:: figures/keyword_arguments.png

When we call the function we will need to specify the value of all variables.
