########################
Arguments default values
########################

.. image:: figures/argument_default_value.png

When we call the function we don't need to specify the value of all variables.
If we don't specify the value of variable, the default value will be taken by default.

**We can also define only some of variables.** 

.. code-block:: python

    my_function(a=1)
