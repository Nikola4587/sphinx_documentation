#####################
Chapiter8: Exceptions
#####################

When something goes wrong and in that moment we catch that exception whe can decide what should happen. 

*****************
Example of errors
*****************

.. code-block:: python

    KeyError
    a_dictionary = {"key": "value"}
    value = a_dictionary["non_existing_key"]

    IndexError
    fruit_list = ["Apple", "Banana", "Pear"]
    fruit = fruit_list[3]

    TypeError
    text = 'abc'
    print(text + 5)
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
*********
Keywords: 
*********

| try:
| except:
| else:
| finally:

| **try:** something that might cause an exception
| **except:** Do this if there was an exception
| **else:** Do this if there were no exception
| **finally:** Do this no matter what happens
| **raise:** Raise our own error

***********
Bad example
***********

.. code-block:: python

    try:
        file = open("a_file.txt")
        a_dictionnary = {"key": "value"}
        print(a_dictionnary["fsadfa"])

    except:
        file = open("a_file.txt", "w")  # If the file is not existing we will create it.

It is not recommended at all to keep except empty. In this case we have the key error in dictionary and we will go directly to except. 
In except we are just creating the txt file, so our key error will completely be ignored.

*********************
Bad example corrected
*********************

To correct this example, in the exception we will catch a specific situation.

.. code-block:: python

    try:
        file = open("a_file.txt")
        a_dictionnary = {"key": "value"}
        print(a_dictionnary["fsadfa"])

    except FileNotFoundError:
        file = open("a_file.txt", "w")  # If the file is not existing we will create it.


If now we run this code, we will have Key Error. We can have multiple exceptions. 

*******************
Multiple exceptions
*******************

.. code-block:: python

    try:
        file = open("a_file.txt")
        a_dictionnary = {"key": "value"}
        print(a_dictionnary["fsadfa"])

    except FileNotFoundError:
        file = open("a_file.txt", "w")  # If the file is not existing we will create it.

    except KeyError:
        print("That key is no exsist")

***************************************
Print the error without crashing a code
***************************************

If we would like to print the error and dont crash: 

.. code-block:: python

    try:
    file = open("a_file.txt")
    a_dictionnary = {"key": "value"}
    print(a_dictionnary["fsadfa"])

    except FileNotFoundError:
        file = open("a_file.txt", "w")  # If the file is not existing we will create it.

    except KeyError as error_message:
        print(f"This key {error_message} is no exist")

************************************
Example using all exception Keywords
************************************

Full example of working with a file:

.. code-block:: python

    try:
        file = open("a_file.txt")
        a_dictionnary = {"key": "value"}
        print(a_dictionnary["key"])

    except FileNotFoundError:
        file = open("a_file.txt", "w")  # If the file is not existing we will create it.

    except KeyError as error_message:
        print(f"This key {error_message} is no exist")

    else:
        content = file.read()
        print(content)

    finally:
        file.close()
        print("File was closed")

*******************
Raise our own error
*******************

.. code-block:: python

    height = float(input("Height"))
    weight = int(input("Weight:"))

    bmi = weight / (height ** 2)

    if height > 3:
        raise ValueError("Human Height should not be over 3 meters.")

If the user enter a heigh bigger than 3m, we will raise a error. 


