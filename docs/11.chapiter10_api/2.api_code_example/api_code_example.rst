#######################################
API Code Example and Errors description
#######################################


************
Code example
************

| Website of ISS current location API: mhttp://open-notify.org/Open-Notify-API/ISS-Location-Now/
| When we open this website, we can found API End Point which is http://api.open-notify.org/iss-now.json. 

.. code-block:: python

    import requests

    response = requests.get(url="http://api.open-notify.org/iss-now.json") # url is the endpoint
    print(response) # At this stage we can not see json data

If we print response, we are getting this message

.. image:: figures/response_message.png
    :width: 200  

Response code: tell us if our request is ok or failed. 

*************************
Response code description
*************************

| 1XX: Hold On. Something is happening, this is not the final. 
| 2XX: Everything is successful. 
| 3XX: You don't have permission to get this think. 
| 4XX: What you are looking for doesn't exist. 
| 5XX: The server that you are making request is getting some issue. Maybe the server is down.

| Link to the all possible response codes: https://www.webfx.com/web-development/glossary/http-status-codes/ 
| What we can do is to catch this response and raise an exception if the response code is not 2XX. 
| In the link above we can see that there is a lot of possible code response, it is very long to take each of them and to raise 
 a error. For this raison we can use **request** module to work with API. This module will raise an exception if something is getting wrong.

**********************
Request module example
**********************

.. code-block:: python

    import requests

    response = requests.get(url="http://api.open-notify.org/iss-now.json") # url is the endpoint

    response.raise_for_status() # Raise an error if there is request error


**********************
Read the data from API
********************** 

.. code-block:: python

    import requests

    response = requests.get(url="http://api.open-notify.org/iss-now.json")  # url is the endpoint
    response.raise_for_status()  # raise an error if there is some issue with a request.
    data = response.json()  # Take the data from API
    longitude = data["iss_position"]["longitude"]
    latitude = data["iss_position"]["latitude"]

    iss_position = (longitude, latitude)
    print(iss_position)
