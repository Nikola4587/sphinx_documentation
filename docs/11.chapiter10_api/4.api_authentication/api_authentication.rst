##################
API Authentication
##################

| Some of API can need to be payed to access to data. 
| For some API, we can also use free account. The way that they prevent people abusing the service is through API KEY. 
 Its personal account number and password. This is how they can track how often we are using their services. 


*******
API KEY
*******

| Open weather map API is needed API key to have access to data. 
| Link to the API: https://openweathermap.org/current 

.. image:: figures/api_key_example.png


If we click on API key, the key will be provided.

| To test if we have access to the api data, we can change lat and lon with our position and replace the key by the one that we have. 
| Once we have the access, we can start to write our programme. 