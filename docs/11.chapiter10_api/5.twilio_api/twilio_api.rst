##########
Twilio API
##########

Twilio API can be used to send sms, whatsapp. 

Link to the documentation: https://www.twilio.com/fr/docs/sms/quickstart/python

************
Code example
************

.. code-block:: python

    from twilio.rest import Client


    account_sid = "AC8ff68c13c43aab3f930688f60f62189f"
    auth_token = "ae3e70e0c3f409cb1f3ec4d2bb3b85c2"


    client = Client(account_sid, auth_token)

    message = client.messages \
                    .create(
                        body="Test.",
                        from_='+19014037629', 
                        to=''
    )
    print(message.status)

Account sid and auth token are coming from Twilio web site - User info.