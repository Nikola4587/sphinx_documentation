#######################
HTTPS Post, Put, Delete
#######################

| Documentation to pixela API: https://pixe.la/
| https://docs.pixe.la/entry/delete-pixel

************
Post example
************

Post: Put some data and dont care about the answer. 

.. code-block:: python

    PIXELA_ENDPOINT = 'https://pixe.la/v1/users'

    user_params = {
        'token': 'tititititi',  # token is character between 8 and 128
        'username': 'toto',
        'agreeTermsOfService': 'yes',
        'notMinor': 'yes'

    }
    response = requests.post(url=PIXELA_ENDPOINT, json=user_params)  # json data that we want to send
    print(response.text)  # Give the response in txt format

*********************
Secure authentication
*********************

.. code-block:: python
    
    # Create the graph
    graph_config = {
        'id': GRAPH_ID,
        'name': 'Cycling graph',
        'unit': 'km',
        'type': 'float',
        'color': 'ajisai'
    }

    headers = {
        "X-USER-TOKEN": TOKEN
    }
    response = requests.post(url=GRAPH_ENDPOINT, json=graph_config, headers=headers)  # headers are kwargs
    print(response.text)

***********
Put example
***********

.. code-block:: python

    headers = {
        "X-USER-TOKEN": TOKEN
    }

    put_config = {
        'quantity': '0.5',
    }
    response = requests.put(f"{POST_PIXEL_ENDPOINT}/{today.strftime('%Y%m%d')}", json=put_config, headers=headers)
    print(response.text)


**************
Delete example
**************

.. code-block:: python

    headers = {
        "X-USER-TOKEN": TOKEN
    }

    # Delete method
    response = requests.delete(f"{POST_PIXEL_ENDPOINT}/{today.strftime('%Y%m%d')}", headers=headers)

