##############
API Parameters
##############


**************
API Parameters
**************

| For some of API we need to set some parameters to be able to read data.
| API Parameters are used to give an input when we are making our API request. 
| If we take a look to "Sunset and sunrise times API", we can see that we have some parameters: 

.. image:: figures/api_parameters_example.png
    :width: 800  

lat and lng parameters are required and the others are optional. 

Here is an example of providing the parameters for Sunset and Sunrise API: 

.. code-block:: python

    parameters = {
    "lat": MY_LAT,
    "lng": MY_LONG,
    "formatted": 0,
    }

    response = requests.get("https://api.sunrise-sunset.org/json", params=parameters)
    response.raise_for_status()
    data = response.json()
    sunrise_time = int(data["results"]["sunrise"].split("T")[1].split(":")[0])
    sunset_time = int(data["results"]["sunset"].split("T")[1].split(":")[0])