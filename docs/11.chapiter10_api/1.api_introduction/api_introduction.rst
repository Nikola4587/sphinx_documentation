###############################################
Application Programing Interface - Introduction
###############################################


*******
Meaning
*******

| An Application Programing Interface (API) is a set of commands, functions, 
 protocols and objects that programmers can use to create software or interact with an external system. 
| For example we can write to Yahoo weather API to get some information about the weather. 
| **Practical example:** 
| Website a restaurant and the data that power this restaurant is the kitchen. We cant go to restaurant and go to kitchen. 
| In a restaurant we have a menu which is a interface. Its tells us what we can order. So in this case API is a menu. 

************
API Endpoint
************

| API Endpoint is a location. If we want a data from a a external service, we need to know what is the location stored. 
 (If we would like to get some money from the bank, we need to know where the bank is.)
| API Endpoint is a url (Everything before the question mark. What is after question mark are the parameters).

***********
API request
***********

| We also need to make a API request over internet. Interface between us and external world. 
| We will make a HTTP request which is get (response = requests.get(url="http://api.open-notify.org/iss-now.json") # url is the endpoint).
| Other common types of requests are: 
| POST (request.post)
| PUT (request.put)
| DELETE (request.delete)

| More details about get, post, put and delete: 
| **Get**: is used when we ask external system for some data and it give us a response. 
| **Post**: We give to external system some piece of data and we are not so interested of response. (Save piece of data in google sheets, post a tweet).
| **Put**: Update piece of data of external service. 
| **Delete**: Delete piece of data of external service. 




